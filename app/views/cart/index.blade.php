@extends('layouts.master')

@section('page-title') @parent

Cart
@stop

@section('content')

<div class="container">


	<h2>Your Shopping Cart</h2>
	@if(count($products) > 0)
		{{ Form::open(['route' => 'cart.update', 'method' => 'put']) }}
		<table class="table table-hover table-bordered">
			<tr>
				<th>Product Name</th>
				<th style="width: 120px;">Unit Price</th>
				<th style="width: 120px;">Quantity</th>
				<th style="width: 120px;">Subtotal</th>
			</tr>
			@foreach ($products as $item)
				<tr>
					<td>
						{{ link_to_route('products.show', $item->product->title, $item->product_id) }}
					</td>
					<td>{{ $item->product->price }}</td>
					<td>{{ Form::text('quantity-cart-' . $item->id, $item->quantity, [
							'class' => 'form-control',
							'size' => '5'
						]) }}</td>
					<td>{{ $item->total }}</td>
				</tr>
			@endforeach
			<tr>
				<td></td>
				<td></td>
				<th>Total</th>
				<td>{{ $total }}</td>
			</tr>
			<tr>
				<td colspan="2"></td>
				<td>{{ Form::submit('Update Cart', ['class' => 'btn btn-default']) }}</td>
				<td></td>
		</table>
		{{ Form::close() }}
	@else
		<p>No products.</p>
	@endif

</div> <!-- /container -->

@stop