<?php


class UserProductCart extends Eloquent {

	var $table = "users_products_cart";

	var $fillable = ['user_id', 'product_id', 'quantity'];

	static $rules = [
		'user_id' => 'required',
		'product_id' => ['required', 'exists:products,id'],
		'quantity' => ['required', 'integer', 'min:1'],
	];


	public static function boot() {
		parent::boot();
		self::observe( new UserProductCartObserver );
	}

	public function product() {
		return $this->belongsTo('Product');
	}

	static public function currentUser() {
		if ( ! Auth::check() ) return new UserProductCartCollection();
		return self::with('product')->where('user_id', Auth::id())->get();
	}

	public function newCollection(array $models = array()) {
		return new UserProductCartCollection($models);
	}

	public function getTotalAttribute() {
		return $this->quantity * $this->product->price;
	}

	public function setQuantityAttribute($value) {
		if ($value != floor($value)) {
			throw new InvalidArgumentException("Quantity must be an integer.");
		}
		$this->attributes['quantity'] = $value;
	}


}

