<?php

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /users
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Display a login form
	 * GET /login
	 * 
	 * @return Response
	*/
	public function login()
	{
		return View::make('users.login')->withError(Session::get('error'));
	}

	/**
	 * Authenticate a user
	 * POST /login
	 * 
	 * @return Response
	*/
	public function authenticate()
	{
		if ( ! Auth::attempt(Input::only('email', 'password')))
		{
			return Redirect::route('login')->withInput()->withError("Nope. Try again.");
		}		return Redirect::intended('/');

	}


	/**
	 * Logout a user
	 * GET /logout
	 * 
	 * @return Response
	*/
	public function logout()
	{
		//
		Auth::logout();
		return Redirect::route('login');
	}

	/**
	 * Show the form for creating a new resource.
	 * GET /users/create
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 * POST /users
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make(
			$data = Input::only('email','password', 'password_confirmation'),
			User::$rules,
			User::$rulesMessages
		);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$user = User::create([
			'email'    => $data['email'],
			'password' => Hash::make($data['password']),
		]);
		Auth::login($user);

		return Redirect::route('products.index');
	}

	/**
	 * Display the specified resource.
	 * GET /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /users/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /users/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}